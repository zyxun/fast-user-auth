<?php
namespace hxhzyh\fastUserAuth;

use Swoole\Coroutine;

class UserAuth
{
    protected $user;
    private static $instance = [];

    static function getInstance(...$args)
    {
        $cid = Coroutine::getCid();
        if(!isset(self::$instance[$cid])){
            self::$instance[$cid] = new static(...$args);
            /*
             * 兼容非携程环境
             */
            if($cid > 0){
                Coroutine::defer(function ()use($cid){
                    unset(self::$instance[$cid]);
                });
            }
        }
        return self::$instance[$cid];
    }
    function destroy(int $cid = null)
    {
        if($cid === null){
            $cid = Coroutine::getCid();
        }
        unset(self::$instance[$cid]);
    }

    public function setUser(array $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getUser($col = null)
    {
        /*if(!$this->user) {
            return null;
        }*/
        if($col === null) {
            return $this->user;
        }
        if(is_string($col)) {
            return isset($this->user[$col]) ? $this->user[$col] : null;
        }
        if(is_array($col)) {
            $returnData = [];
            foreach ($col as $v) {
                if(!is_string($v)) continue;
                if(isset($this->user[$v])) {
                    $returnData[$v] = $this->user[$v];
                }
            }
            return $returnData;
        }

        return $this->user;
    }
}